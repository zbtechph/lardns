<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDnsZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dns_zones', function (Blueprint $table) {
            $table->id();
            $table->string("domain")->unique();
            $table->string("provider");
            $table->boolean("status")->default(true);
            $table->foreignId("user_id");
            $table->json("metas")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dns_zones');
    }
}
