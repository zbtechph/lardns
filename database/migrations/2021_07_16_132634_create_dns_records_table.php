<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDnsRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dns_records', function (Blueprint $table) {
            $table->id();
            $table->foreignId("dns_zone_id");
            $table->string("name");
            $table->string("content");
            $table->enum("type", [
                "A", "AAAA", "CNAME", "TXT", "MX"
            ]);
            $table->foreignId("user_id");
            $table->json("metas")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dns_records');
    }
}
