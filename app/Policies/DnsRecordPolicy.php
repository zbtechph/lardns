<?php

namespace App\Policies;

use App\Models\DnsRecord;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DnsRecordPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        //
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\DnsRecord  $dnsRecord
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, DnsRecord $dnsRecord)
    {
        //
        return $user->id === $dnsRecord->user_id
            OR $user->id === $dnsRecord->dnsZone->user_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        //
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\DnsRecord  $dnsRecord
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, DnsRecord $dnsRecord)
    {
        //
        return $user->id === $dnsRecord->user_id
            OR $user->id === $dnsRecord->dnsZone->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\DnsRecord  $dnsRecord
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, DnsRecord $dnsRecord)
    {
        //
        return $user->id === $dnsRecord->user_id
            OR $user->id === $dnsRecord->dnsZone->user_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\DnsRecord  $dnsRecord
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, DnsRecord $dnsRecord)
    {
        //
        return $user->id === $dnsRecord->user_id
            OR $user->id === $dnsRecord->dnsZone->user_id;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\DnsRecord  $dnsRecord
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, DnsRecord $dnsRecord)
    {
        //
        return $user->id === $dnsRecord->user_id
            OR $user->id === $dnsRecord->dnsZone->user_id;
    }
}
