<?php

namespace App\Http\Requests;

use Faker\Core\Number;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Ramsey\Uuid\Type\Integer;

class StoreDnsZoneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "domain" => [
                "required",
                "unique:dns_zones"
            ],
            "provider" => [
                "string",
                "required"
            ],
            "status" => [
                "boolean"
            ]
        ];
    }
}
