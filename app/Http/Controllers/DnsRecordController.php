<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDnsRecordRequest;
use App\Http\Requests\UpdateDnsRecordRequest;
use App\Models\DnsRecord;
use Illuminate\Support\Facades\Auth;

class DnsRecordController extends Controller
{

    public function __construct(){
        $this->authorizeResource(DnsRecord::class, "record");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Auth::user()->dnsRecords;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDnsRecordRequest $request)
    {
        return $request->user()->dnsRecords()->create($request->validated());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DnsRecord  $dnsRecord
     * @return \Illuminate\Http\Response
     */
    public function show(DnsRecord $dnsRecord)
    {
        return $dnsRecord;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DnsRecord  $dnsRecord
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDnsRecordRequest $request, DnsRecord $dnsRecord)
    {
        return $dnsRecord->update($request->validated());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DnsRecord  $dnsRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(DnsRecord $dnsRecord)
    {
        //
        return $dnsRecord->delete();
    }
}
