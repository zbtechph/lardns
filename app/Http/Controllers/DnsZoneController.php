<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDnsZoneRequest;
use App\Http\Requests\UpdateDnsZoneRequest;
use App\Models\DnsZone;
use Illuminate\Support\Facades\Auth;

class DnsZoneController extends Controller
{

    public function __construct(){
        $this->authorizeResource(DnsZone::class, "zone");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Auth::user()->dnsZones;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDnsZoneRequest $request)
    {
        $data = $request->validated();
        return $request->user()->dnsZones()->create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DnsZone  $dnsZone
     * @return \Illuminate\Http\Response
     */
    public function show(DnsZone $dnsZone)
    {
        return $dnsZone;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DnsZone  $dnsZone
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDnsZoneRequest $request, DnsZone $dnsZone)
    {
        return $dnsZone->update($request->validated());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DnsZone  $dnsZone
     * @return \Illuminate\Http\Response
     */
    public function destroy(DnsZone $dnsZone)
    {
        return $dnsZone->delete();
    }
}
