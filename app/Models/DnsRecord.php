<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DnsRecord extends Model
{
    use HasFactory;

    protected $guarded = [
        "user_id"
    ];

    public function dnsZone(){
        return $this->belongsTo(DnsZone::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

}
