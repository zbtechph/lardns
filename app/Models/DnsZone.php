<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DnsZone extends Model
{
    use HasFactory;

    protected $guarded = [
        "user_id"
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function dnsRecords(){
        return $this->hasMany(DnsRecord::class);
    }

}
